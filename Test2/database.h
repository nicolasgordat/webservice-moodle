#ifndef DATABASE_H
#define DATABASE_H

#include <QtSql/QtSql>
#include <QtXml/QtXml>

/* Database Infos
 */
#define SQL_HOSTNAME "localhost"
#define SQL_USERNAME "root"
#define SQL_PASSWORD "isdn&inter"
#define PREFIXE "mdl_"

class Database
{

private:
    QString hostname;
    QString username;
    QString password;
    QSqlDatabase qtdb;

    /* Setters */
    bool  setUsername(QString username);
    bool  setPassword(QString password);
    bool  setHostname(QString hostname);

public:
    Database();
    Database(QString hostname);
    Database(QString hostname, QString username, QString password);
    ~Database();

    /* Change login infos */
    bool setUser(QString username, QString password);

    /* Initialize a MySQL connection */
    bool startConnexion(QString database);
};

#endif // DATABASE_H
