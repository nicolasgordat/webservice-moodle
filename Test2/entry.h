#ifndef ENTRY_H
#define ENTRY_H

#include <qstringlist.h>

class Entry
{
public:
    Entry();

    /* Getters */
    QString getColumn(int id);
    QString getValue(int id);
    int getLength();

    /* Setters */
    bool setEntry(QString column, QString value);
    bool setEntry(QStringList column, QStringList value);

private:
    QStringList columns;
    QStringList values;

    /* Setters */
    bool setColumn(QString str);
    bool setValue(QString str);
};

#endif // ENTRY_H
