#include "database.h"
#include <QtSql/QtSql>
#include <QtXml/QtXml>

/* Constructors
 */
Database::Database()
{
    this->setHostname(SQL_HOSTNAME);
    this->setUser(SQL_USERNAME, SQL_PASSWORD);
}

Database::Database(QString hostname)
{
    this->setHostname(hostname);
}

Database::Database(QString hostname, QString username, QString password)
{
    this->setHostname(hostname);
    this->setUsername(username);
    this->setPassword(password);
}


/* Destructor
 */
Database::~Database()
{
    if(this->qtdb.isOpen()) this->qtdb.close();
}


/* Setters
 */
bool Database::setHostname(QString hostname)
{
    this->hostname = hostname;
    return true;
}

bool Database::setUsername(QString username)
{
    this->username = username;
    return true;
}

bool Database::setPassword(QString password)
{
    this->password = password;
    return true;
}

bool Database::setUser(QString username, QString password)
{
    this->setUsername(username);
    this->setPassword(password);
    return true;
}


/* Initialize a MySQL connection */
bool Database::startConnexion(QString database)
{
    if (!qtdb.isValid()) {
        this->qtdb = QSqlDatabase::addDatabase("QMYSQL");
        this->qtdb.setHostName(SQL_HOSTNAME);
        this->qtdb.setUserName(SQL_USERNAME);
        this->qtdb.setPassword(SQL_PASSWORD);
        this->qtdb.setDatabaseName(database);
        this->qtdb.open();
    }
    return this->qtdb.isOpen();
}
