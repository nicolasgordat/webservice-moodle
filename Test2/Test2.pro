TARGET = Test2
TEMPLATE = lib

QT -= gui
QT += core
QT += sql
QT += xml
CONFIG += plugin TUFAO0

DEFINES += PLUGIN=Test2

SOURCES += plugin.cpp \
    requesthandler.cpp \
    database.cpp \
    entry.cpp

HEADERS += plugin.h \
    requesthandler.h \
    database.h \
    entry.h
