#include "entry.h"

/* Constructor
 */
Entry::Entry()
{
    this->columns = QStringList();
    this->values = QStringList();
}


/* Getters
 */
QString Entry::getColumn(int id)
{
    if (id < this->columns.length())
    {
        return this->columns.at(id);
    }
    return "";
}

QString Entry::getValue(int id)
{
    if (id < this->values.length())
    {
        return this->values.at(id);
    }
    return "";
}

int Entry::getLength()
{
    // TODO: A perfectionner pour que ca retourne le minimum des 2
    return this->columns.length();
}

/* Setters
 */
bool Entry::setColumn(QString str)
{
    this->columns.append(str);
    return true;
}

bool Entry::setValue(QString str)
{
    this->values.append(str);
    return true;
}

bool Entry::setEntry(QString column, QString value)
{
    this->setColumn(column);
    this->setValue(value);
    return true;
}

bool Entry::setEntry(QStringList column, QStringList value)
{
    int i;
    if (column.length() != value.length()) return false;
    for (i=0; i<column.length(); i++)
    {
        this->setEntry(column.at(i), value.at(i));
    }
    return true;
}
