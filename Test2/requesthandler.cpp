#include "requesthandler.h"
#include <Tufao/HttpServerRequest>
#include <QtXml/QtXml>


/* Constructors
 */
RequestHandler::RequestHandler(QObject *parent) :
    Tufao::AbstractHttpServerRequestHandler(parent)
{
    this->domain = QString();
    this->table = QString();
    this->params = QStringList();
    this->sqlConnexion = Database();
    this->buffer = QByteArray();
}


/* Basic operations
 */
bool RequestHandler::isNumber(const QString str)
{
    std::string s = str.toStdString();
    std::string::const_iterator it = s.begin();
    while (it != s.end() && std::isdigit(*it)) ++it;
    return !s.empty() && it == s.end();
}

bool RequestHandler::isStr(const QString &string, bool AcceptSpace)
{
    int i;
    bool result = TRUE;
    for (i=0;i<string.length();i++)
        if (AcceptSpace)
        {
            if (string.at(i).toAscii() == 32 ||
                (string.at(i).toAscii() >= 65 && string.at(i).toAscii() <= 90) ||
                (string.at(i).toAscii() >= 97 && string.at(i).toAscii() <= 122))
                result = FALSE;
        }
        else
        {
            if ((string.at(i).toAscii() >= 65 && string.at(i).toAscii() <= 90) ||
                (string.at(i).toAscii() >= 97 && string.at(i).toAscii() <= 122))
                result = FALSE;
        }

    return result;
}


/* SQL Handling
 * Uses the Database class to interact with MySQL servers and the Entry class to store user's request
 * Able to make all SQL basic operations and sending result.
 * If you want to change the domains allowed, see RequestHandler::getDatabaseName;
 */
bool RequestHandler::select()
{
    if (!this->sqlConnexion.startConnexion(this->getDatabaseName(this->domain)))
    {
        this->quickAnswer(200, QByteArray("Erreur de connexion"));
        return true;
    }

    QSqlQuery query;
    if(query.exec(QString("SELECT * FROM `").append(PREFIXE).append(this->table).append("`")))
    {
        this->domDocumentAnswer(this->formatResponse(query));
    }
    else this->domDocumentAnswer(this->formatError(query));

    return true;
}

bool RequestHandler::select(int id)
{
    if (!this->sqlConnexion.startConnexion(this->getDatabaseName(this->domain)))
    {
        this->quickAnswer(200, QByteArray("Erreur de connexion"));
        return true;
    }

    QSqlQuery query;
    if(query.exec(QString("SELECT * FROM `").append(PREFIXE).append(this->table).append("` WHERE `id`=").append(QString::number(id))))
    {
        if(query.size() > 0) this->domDocumentAnswer(this->formatResponse(query));
        else this->domDocumentAnswer(this->noResultResponse());
    }
    else this->domDocumentAnswer(this->formatError(query));

    return true;
}

bool RequestHandler::insert(Entry entry)
{
    if (!this->sqlConnexion.startConnexion(this->getDatabaseName(this->domain)))
    {
        this->quickAnswer(200, QByteArray("Erreur de connexion"));
        return true;
    }

    QSqlQuery query;
    QString stringQuery;

    stringQuery = QString("INSERT INTO `").append(PREFIXE).append(this->table).append("` ");
    stringQuery.append("(");
    for (int i=0; i < entry.getLength(); i++)
    {
        stringQuery.append("`").append(entry.getColumn(i)).append("`");
        if (i != entry.getLength() - 1) stringQuery.append(", ");
    }
    stringQuery.append(") VALUES (");
    for (int i=0; i < entry.getLength(); i++)
    {
        stringQuery.append("\"").append(entry.getValue(i)).append("\"");
        if (i != entry.getLength() - 1) stringQuery.append(", ");
    }
    stringQuery.append(")");

    if(query.exec(stringQuery))
    {
        if(query.numRowsAffected() > 0) this->domDocumentAnswer(this->succesResponse(query));
        else this->domDocumentAnswer(this->noResultResponse());
    }
    else this->domDocumentAnswer(this->formatError(query));

    return true;
}

bool RequestHandler::update(Entry entry, int id)
{
    if (!this->sqlConnexion.startConnexion(this->getDatabaseName(this->domain)))
    {
        this->quickAnswer(200, QByteArray("Erreur de connexion"));
        return true;
    }

    QSqlQuery query;
    QString stringQuery;
    stringQuery = QString("UPDATE `").append(PREFIXE).append(this->table).append("` SET ");
    for (int i=0; i < entry.getLength(); i++)
    {
        stringQuery.append("`").append(entry.getColumn(i)).append("` = \"").append(entry.getValue(i)).append("\"");
        if (i != entry.getLength() - 1) stringQuery.append(", ");
    }
    stringQuery.append(" WHERE `id` = ").append(QString::number(id));

    if(query.exec(stringQuery))
    {
        if(query.numRowsAffected() > 0) this->domDocumentAnswer(this->succesResponse(query));
        else this->domDocumentAnswer(this->noResultResponse());
    }
    else this->domDocumentAnswer(this->formatError(query));

    return true;
}

bool RequestHandler::remove(int id)
{
    if (!this->sqlConnexion.startConnexion(this->getDatabaseName(this->domain)))
    {
        this->quickAnswer(200, QByteArray("Erreur de connexion"));
        return true;
    }

    QSqlQuery query;
    query = QSqlQuery();
    if(query.exec(QString("DELETE FROM `").append(PREFIXE).append(this->table).append("` WHERE `id`=").append(QString::number(id))))
    {
        if(query.numRowsAffected() > 0) this->domDocumentAnswer(this->succesResponse(query));
        else this->domDocumentAnswer(this->noResultResponse());
    }
    else this->domDocumentAnswer(this->formatError(query));

    return true;
}

QString RequestHandler::getDatabaseName(QString niveau)
{
    if (niveau.compare("cir") == 0) return "Moodle";
    else if (niveau.compare("csi") == 0) return "Moodle";
    else if (niveau.compare("3") == 0) return "Moodle";
    else if (niveau.compare("m") == 0) return "Moodle";
    else return "Moodle";
}


/* Security
 * It ensures that anybody can't access to the webservice
 */
bool RequestHandler::isAuthorized(const QStringList &string)
{
    if (string.length() >= 2)
    {
        if (string.at(0) == USERNAME && string.at(1) == PASSWORD) return true;
        else return false;
    }
    else return false;
}


/* Response Handling
 * It is responsible for sending answer to the client
 */
bool RequestHandler::quickAnswer(int statut, QByteArray reason)
{
    this->response->writeHead(statut, reason);
    this->response->end(reason + "\n");
    return true;
}

bool RequestHandler::domDocumentAnswer(QDomDocument document)
{
    this->response->writeHead(200);
    this->response->end(document.toByteArray());
    return true;
}

/* Response Parsing & Build-in response
 */
QDomDocument RequestHandler::noResultResponse()
{
    QDomDocument document;
    QDomElement root, description;

    root = document.createElement("query");
    document.appendChild(root);

    description = document.createElement("no-result");
    description.setAttribute("result", "0");
    root.appendChild(description);

    return document;
}

QDomDocument RequestHandler::succesResponse(QSqlQuery query)
{
    QDomDocument document;
    QDomElement root, description;

    root = document.createElement("query");
    document.appendChild(root);

    description = document.createElement("result");
    description.setAttribute("result", query.numRowsAffected());
    root.appendChild(description);

    return document;
}

QDomDocument RequestHandler::formatResponse(QSqlQuery query)
{
    QStringList listeColums;
    QDomDocument document;
    QDomElement root, description;
    QSqlRecord record;
    QDomElement temp, container;
    int i;

    root = document.createElement("query");
    document.appendChild(root);

    record = query.record();

    description = document.createElement("description");
    root.appendChild(description);

    for(i=0; i < record.count(); i++)
    {
        temp = document.createElement("name");
        listeColums.append(record.fieldName(i));
        temp.setAttribute("name", record.fieldName(i));
        description.appendChild(temp);
    }

    while(query.next())
    {
        container = document.createElement("result");
        for(int x=0; x < query.record().count(); x++)
        {
            temp = document.createElement(listeColums.at(x));
            temp.setAttribute("value", query.value(x).toString());
            container.appendChild(temp);
        }
        root.appendChild(container);
    }

    return document;
}

QDomDocument RequestHandler::formatError(QSqlQuery query)
{
    QDomDocument document;
    QDomElement root, error;

    root = document.createElement("query");
    document.appendChild(root);

    error = document.createElement("error");
    error.setAttribute("value", query.lastError().databaseText());
    root.appendChild(error);

    return document;
}

/* POST data handling
 * Functions dedicated to handle POST data and to make it an Entry
 */
void RequestHandler::onData(const QByteArray &chunk)
{
    buffer += chunk;
}

void RequestHandler::onEnd()
{
    // Si on a un id -> UPDATE, sinon -> INSERT
    if(this->params.isEmpty() || this->params.first() == "") this->insert(this->parseData(buffer));
    else if(isNumber(this->params.at(0))) this->update(this->parseData(buffer), params.at(0).toInt());
}

Entry RequestHandler::parseData(QByteArray buffer)
{
    Entry entry;
    QStringList liste, columns, values, temp;
    QString arguments = QString(buffer);
    int i;

    liste = arguments.split("&");
    for (i=0; i < liste.length(); i++)
    {
        temp = liste.at(i).split("=");
        if (temp.length() == 2)
        {
            columns.append(temp.at(0));
            values.append(temp.at(1));
        }
    }

    entry.setEntry(columns, values);

    return entry;
}

bool RequestHandler::connectPost(Tufao::HttpServerRequest *request)
{
    connect(request, SIGNAL(data(QByteArray)), this, SLOT(onData(QByteArray)));
    connect(request, SIGNAL(end()), this, SLOT(onEnd()));
    return true;
}


/* Request Handler
 * Function called when a new request that suit plugin's regex pattern is received
 */
bool RequestHandler::handleRequest(Tufao::HttpServerRequest *request, Tufao::HttpServerResponse *response, const QStringList &args)
{
    this->response = response;

    QString username, password;

    /*
     *DEFINITION des commandes
     *
     *V GET
     *  V Get + "user/password/auth" -> renvoie true or false
     *  V Get: Recupérer la liste complete
     *  V Get + id: Recupérer les infos de cet id
     *
     *POST
     *  V Post: Nouvelle entrée
     *  Post + id: Update l'entrée id
     *
     *V DELETE
     *  V Delete + id: Supprime l'entrée id
     *
     *    ------
     *
     * Schema de requete : username/password/moodle/table/suite des parametres ...
     */

    // Parameters required
    if (args.isEmpty() || args.length() < 3)
    {
        // Code 400: Bad Request
        quickAnswer(400, "Bad Request");
        return true;
    }
    else if (!isAuthorized(args))
    {
        // Code 403: Not authorized
        quickAnswer(403, "Not authorized");
        return true;
    }

    // Domain : will define the database
    this->domain = args.at(2);
    if (this->domain == "auth")
    {
        quickAnswer(200, "true");
        return true;
    }

    // A partir d'ici les args n'existent pas forcement
    //Table : will define the table on which we will work
    if (args.length() >= 4) this->table = args.at(3);
    else this->table = "";
    // Params beetween /
    if (args.length() >= 5) this->params = args.at(4).split('/');
    else this->params = QStringList("");

    // Methode detection
    if (request->method() == QByteArray("GET"))
    {
        if(params.isEmpty() || params.first() == "") this->select();
        else if(isNumber(params.at(0))) this->select(params.at(0).toInt());
        else  {
            // Code 400: Bad Request
            quickAnswer(400, "Bad Request");
            return true;
        }
    }
    else if (request->method() == QByteArray("POST"))
    {
        this->connectPost(request);
        return true;
    }
    else if (request->method() == QByteArray("DELETE"))
    {
        if(params.empty()) quickAnswer(403, "Not authorized");
        else if(isNumber(params.at(0))) this->remove(params.at(0).toInt());
        else {
            // Code 400: Bad Request
            quickAnswer(400, "Bad Request");
            return true;
        }
    }
    else
    {
        // Code 405: Method Not Allowed
        quickAnswer(405, "Method Not Allowed");
    }

    return true;
}


/* Destructor */
RequestHandler::~RequestHandler()
{
    qDebug("Destruction du gestionnaire de Request");
}
