#ifndef REQUESTHANDLER_H
#define REQUESTHANDLER_H

#include <Tufao/AbstractHttpServerRequestHandler>
#include <qstring.h>
#include <qstringlist.h>
#include "database.h"
#include "entry.h"
#include <QtXml/QtXml>

#define USERNAME "rossini"
#define PASSWORD "1234"

class RequestHandler: public Tufao::AbstractHttpServerRequestHandler
{
    Q_OBJECT

private:
    QByteArray buffer;
    QString domain;
    QString table;
    QStringList params;
    Database sqlConnexion;
    QDomDocument xmlAnswer;
    Entry entryPost;
    Tufao::HttpServerResponse *response;

    /* Basic operations */
    bool isNumber(const QString str);
    bool isStr(const QString &string, bool AcceptSpace);

    /* Security */
    bool isAuthorized(const QStringList &string);

    /* Response Handling */
    bool domDocumentAnswer(QDomDocument document);
    bool quickAnswer(int statut, QByteArray reason);

    /* SQL Handling */
    bool select();
    bool select(int id);
    bool insert(Entry entry);
    bool update(Entry entry, int id);
    bool remove(int id);
    QString getDatabaseName(QString niveau);

    /* POST data Handling */
    bool connectPost(Tufao::HttpServerRequest *request);

public:
    explicit RequestHandler(QObject *parent = 0);

    /* Response Parsing & Build-in response */
    static QDomDocument noResultResponse();
    static QDomDocument succesResponse(QSqlQuery query);
    QDomDocument formatResponse(QSqlQuery query);
    QDomDocument formatError(QSqlQuery query);
    Entry parseData(QByteArray buffer);

    ~RequestHandler();
public slots:
    /* Request Handling */
    bool handleRequest(Tufao::HttpServerRequest *request, Tufao::HttpServerResponse *response, const QStringList &args = QStringList());

private slots:

    /* POST data Handling */
    void onData(const QByteArray &chunk);
    void onEnd();
};

#endif // REQUESTHANDLER_H
